#include "../stdafx.h"

#include "../tbtr_template_vehicle.h"
#include "../tbtr_template_vehicle_func.h"

#include "../train.h"

#include "saveload.h"

const SaveLoad* GTD() {

	static const SaveLoad _template_veh_desc[] = {
		SLE_CONDREF(TemplateVehicle, next, 		REF_TEMPLATE_VEHICLE, TBTR_SV, SL_MAX_VERSION),

		SLE_CONDVAR(TemplateVehicle, reuse_depot_vehicles, SLE_UINT8, TBTR_SV, SL_MAX_VERSION),
		SLE_CONDVAR(TemplateVehicle, keep_remaining_vehicles, SLE_UINT8, TBTR_SV, SL_MAX_VERSION),
		SLE_CONDVAR(TemplateVehicle, refit_as_template, SLE_UINT8, TBTR_SV, SL_MAX_VERSION),

		SLE_CONDVAR(TemplateVehicle, owner, SLE_UINT32, TBTR_SV, SL_MAX_VERSION),
		SLE_CONDVAR(TemplateVehicle, owner_b, SLE_UINT8, TBTR_SV, SL_MAX_VERSION),

		SLE_CONDVAR(TemplateVehicle, engine_type, SLE_UINT16, TBTR_SV, SL_MAX_VERSION),
		SLE_CONDVAR(TemplateVehicle, cargo_type, SLE_UINT8, TBTR_SV, SL_MAX_VERSION),
		SLE_CONDVAR(TemplateVehicle, cargo_cap, SLE_UINT16, TBTR_SV, SL_MAX_VERSION),
		SLE_CONDVAR(TemplateVehicle, cargo_subtype, SLE_UINT8, TBTR_SV, SL_MAX_VERSION),

		SLE_CONDVAR(TemplateVehicle, subtype, SLE_UINT8, TBTR_SV, SL_MAX_VERSION),
		SLE_CONDVAR(TemplateVehicle, railtype, SLE_UINT8, TBTR_SV, SL_MAX_VERSION),

		SLE_CONDVAR(TemplateVehicle, index, SLE_UINT32, TBTR_SV, SL_MAX_VERSION),

		SLE_CONDVAR(TemplateVehicle, real_consist_length, SLE_UINT16, TBTR_SV, SL_MAX_VERSION),

		SLE_CONDVAR(TemplateVehicle, max_speed, SLE_UINT16, TBTR_SV, SL_MAX_VERSION),
		SLE_CONDVAR(TemplateVehicle, power, SLE_UINT32, TBTR_SV, SL_MAX_VERSION),
		SLE_CONDVAR(TemplateVehicle, weight, SLE_UINT32, TBTR_SV, SL_MAX_VERSION),
		SLE_CONDVAR(TemplateVehicle, max_te, SLE_UINT32, TBTR_SV, SL_MAX_VERSION),

		SLE_CONDVAR(TemplateVehicle, spritenum, SLE_UINT8, TBTR_SV, SL_MAX_VERSION),
		SLE_CONDVAR(TemplateVehicle, cur_image, SLE_UINT32, TBTR_SV, KK_PATCHPACK_SV_2_195),
		SLE_CONDVAR(TemplateVehicle, image_width, SLE_UINT32, TBTR_SV, KK_PATCHPACK_SV_2_195),

		SLE_END()
	};

	static const SaveLoad * const _ret[] = {
		_template_veh_desc,
	};

	return _ret[0];
}

static void Save_TMPLS()
{
	TemplateVehicle *tv;

	FOR_ALL_TEMPLATES(tv) {
		SlSetArrayIndex(tv->index);
		SlObject(tv, GTD());
	}
}

static void Load_TMPLS()
{
	int index;

	while ((index = SlIterateArray()) != -1) {
		TemplateVehicle *tv = new (index) TemplateVehicle(); //TODO:check with veh sl code
		SlObject(tv, GTD());
	}
}

static void Ptrs_TMPLS()
{
	TemplateVehicle *tv;
	FOR_ALL_TEMPLATES(tv) {
		SlObject(tv, GTD());
	}
}

void AfterLoadTemplateVehicles()
{
	TemplateVehicle *tv;

	FOR_ALL_TEMPLATES(tv) {
		/* Reinstate the previous pointer */
		if (tv->next != NULL) tv->next->previous = tv;
		tv->first =NULL;
	}
	FOR_ALL_TEMPLATES(tv) {
		/* Fill the first pointers */
		if (tv->previous == NULL) {
			for (TemplateVehicle *u = tv; u != NULL; u = u->Next()) {
				u->first = tv;
			}
		}
	}
}

void UpdateTemplateSprites()
{
	TemplateVehicle *tv;
	FOR_ALL_TEMPLATES(tv) {
		Train *t = VirtualTrainFromTemplateVehicle(tv);
		VehicleSpriteSeq seq;
		t->GetImage(DIR_W, EIT_PURCHASE, &seq);
		tv->cur_image = seq.seq[0].sprite;
		tv->image_width = t->GetDisplayImageWidth(NULL);
		delete t;
	}
}

extern const ChunkHandler _template_vehicle_chunk_handlers[] = {
	{'TMPL', Save_TMPLS, Load_TMPLS, Ptrs_TMPLS, NULL, CH_ARRAY | CH_LAST},
};
