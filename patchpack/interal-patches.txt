Always show wagon speed limit

2cc trainset contains a feature that lets users run wagons above their original
speed limit. For this, OpenTTD's speed limit has to be disabled. If this is
done, then maintenance costs of the wagons will be increased (a little bit or a
whole lot, depending on the speed at which they are used).
OpenTTD would not show wagon speed limit in the new vehicle dialog when speed
limit was disabled, which was rather annoying in combination with 2cc's dynamic
maintenance cost feature, so we made the buy vehicle dialog always show the
wagon speed limit.



Permissive station building

Generally, towns will allow to build rails and roads, but not stations if a
companie's ratings are very poor or less, which would make it extremely annoying
building rails or roads first and then stations (i.e. in case rails were built
near a town previously), but the town is to be included in the network only now.
This patch adds a setting to always allow companies to build stations in towns
no matter the ratings. The (in)ability to demolish town buildings with poor
ratings remains unaffected.



Remove water animations

Added a setting (environment) that allows to disable the water animations (which
would cause the screen to flicker annoyingly when fast-forwarding)



Inflation remover

Added a cheat option to remove inflation (i.e. reset everything to base price)



Daylength hack

An alternative daylength hack. Instead of increasing the ticks per day (which
may lead to things like extended subsidy times, or extended timeframe to deliver
multiple products / supplies to an industry to increase production, this hack
will go through a year a configurable amount of times until advancing to the
next one.
Stuff that should happen once a year at a specific point in time (introduction
of new vehicles, offer of preview of vehicles, applying inflation, aging houses,
deciding whether to use new semaphores) will happen in the last repetition of
that year.
Other stuff (aging of cargo and individual vehicles) will still happen normally.
One thing of note should be how display of different values is handled that
normally address a year. Generally, anything that will be accumulated over a
year and reset at its end (i.e. moving vehicle profits) will be accumulated
over all repetitions of a year and reset after the last repetition.
To ease calculating amortized costs for vehicles, the profits _last year_ will
not refer to the accumulated costs of the last year. Rather, all profits that
the vehicle accumulated over all repetitions of the last year will be averaged
over the number of repetitions, so this number will show the average profit that
the vehicle made.



Linear coastline bias

Improves the -kind of- lacking "force water on map edge" function contained in
OpenTTD's TGP terrain generator. Instead of just cutting into the readily
generated map from the edges after water height level has already been selected,
we apply a linear bias to the map height before the water level is selected.
The linear bias has two configurable values: Strength and range. The strength
determines the maximum height (before any smoothing or companding takes place)
that is ever added to or removed from a single field's height. The strength is
configured as a value from 0 to 80, where 40 represents no strength at all.
Selecting a value above 40 will decrease altitude towards the map edge,
selecting a value below 40 will increase it. The exact maximum value is
determined by subtracting 40 from the configured value and dividing it by 10.
The result is multiplied to the difference between average and minimum altitude
on the generated map to obtain the maximum value that is subtracted from or
added to the map's altitude.
The range determines how far from the map edge we must go for the bias to lose
its effect (configured in 1/16th of the length of the axis perpendicular to the
map edge in question).
At the map edge, the maximum value of the bias is applied. From there, the value
decreases linearly until the distance configured by the range option is reached.
It is guaranteed that if two adjacent edges are selected as coastline, the bias
will only be applied once to each field (even if the areas where the individual
biases for both edges overlap).